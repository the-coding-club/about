var gulp = require('gulp'),
  server = require('gulp-server-livereload'),
  sass = require('gulp-sass'),
  nunjucksRender = require('gulp-nunjucks-render'),
  fm = require('front-matter'),
  notifier = require('node-notifier')
  notify = require('gulp-notify'),
  gutil = require('gulp-util'),
  data = require('gulp-data'),
  htmlmin = require('gulp-htmlmin'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  fs = require('fs');

function getPageListing(){
  var folders=[];
  fs.readdirSync('pages').forEach(file => {
    var nameParts=file.split('_');
    for(var i=0;i<nameParts.length;i++){
      nameParts[i]=nameParts[i][0].toUpperCase()+nameParts[i].slice(1);
    }
    folders.push({
      folderName:file,
      name:nameParts.join(" ")
    });
  });
  return {
    folders: folders
  };
}

gulp.task('index',function(){
  return gulp.src(['index.html'])
    .pipe(data(getPageListing()))
    .pipe(nunjucksRender())
    .pipe(gulp.dest('build'));
});

gulp.task('pages',function(){
  return gulp.src(['pages/**'])
    .pipe(gulp.dest('build/pages'));
});

gulp.task('build',['index','pages']);

gulp.task('watch', function() {
  gulp.watch(['index.html','pages/**'], ['index','pages']);
})

gulp.task('serve', ['build'], function() {
  gulp.src('build')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});

gulp.task('default',['build','serve','watch']);
