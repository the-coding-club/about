# About Page Project

This project contains the about pages of participants of the coding club. Every participant should

- clone this repository
- create branch firstname_lastname
- add a new folder firstname_lastname in the pages directory
- add their about page (imp, there must be an index.html file)
- commit and push
- create merge request
- ask coach to review
- after approval, merge to master

## Building the project locally

- install node (if not already installed)
- install gulp (if not already installed)

In folder execute

- ´npm install´
- ´gulp´
